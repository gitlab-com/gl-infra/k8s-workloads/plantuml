#!/usr/bin/env bash
# vim: ai:ts=2:sw=2:et

##############################
# wrapper script for running
# helm against this chart

set -euf -o pipefail
dir="$(
  cd "$(dirname "${0}")"
  pwd
)"

CHART="${CHART:-plantuml}"

export HELM_TILLER_LOGS=true
export HELM_TILLER_LOGS_DIR=${PWD}/tiller.log
export SERVER_HOST="${CI_SERVER_HOST:-gitlab.com}"

# Source common functions and variable exports
# that are common to all charts

COMMON_SCRIPT_PATH="${COMMON_SCRIPT_PATH:-/k8s-workloads/common.bash}"
if [[ -r "$COMMON_SCRIPT_PATH" ]]; then
  source "$COMMON_SCRIPT_PATH"
else
  # Grab the CI image version from  the .gitlab-ci.yml
  _BBLK="\\033[1;30m"
  _NORM="\\033[0m"
  version=$(sed -E -e "s/[[:space:]]+CI_IMAGE_VERSION:[[:space:]]'(v[0-9\\.]+)'/\\1/" -e "t" -e "d" .gitlab-ci.yml)
  echo -e "${_BBLK}Sourcing version $version of the common shell script"
  echo -e "if you want to use a local version set COMMON_SCRIPT_PATH to the location of common.bash"
  echo -e "or update .gitlab-ci.yml to set a new version${_NORM}"
  echo ""
  source <(curl -s "https://$SERVER_HOST/gitlab-com/gl-infra/k8s-workloads/common/raw/$version/bin/common.bash")
fi

NAME="${NAME:-plantuml${STAGE_SUFFIX}}"
NAMESPACE="${NAMESPACE:-plantuml${STAGE_SUFFIX}}"
CHART_VERSION=${CHART_VERSION:-$(get_chart_version)}
CHARTS_DIR=$(mktemp -d)
EXTRACTED_CHARTS_DIR="${CHARTS_DIR}/${CHART}"
MANIFESTS_DIR="$dir/../manifests"

get_plantuml_chart() {
  if is_semver "$CHART_VERSION"; then
    if install_plantuml_chart_via_helm; then
      chart_install_method=helm
    elif install_plantuml_chart_via_s3; then
      chart_install_method=s3
    else
      debug "Unable to install chart using semver!" "${_RED}"
      exit 1
    fi
  else
    install_plantuml_chart_via_git
    chart_install_method=git
    # For non-semvers, attempt to install from git
    # and override CHART_VERSION to version+ref
    # Example: 3.1.0+0035def7
    CHART_VERSION="$CHART_VERSION_SHA"
  fi
  debug "Using install method: $chart_install_method"
  debug "Setting CHART_VERSION=$CHART_VERSION" "${_YEL}"
}

is_semver() {
  if [[ $1 =~ ^v?[0-9]+\.[0-9]+\.[0-9]+ ]]; then
    return 0
  else
    return 1
  fi
}

install_plantuml_chart_via_git() {
  local chart_ref="$CHART_VERSION"

  git clone -b "${chart_ref}" "https://gitlab.com/gitlab-org/charts/$CHART.git" "$EXTRACTED_CHARTS_DIR" >/dev/null

  pushd "$EXTRACTED_CHARTS_DIR"

  local version sha chart_version_sha
  version=$(grep version Chart.yaml | awk '{print $2}')
  sha=$(git rev-parse --short HEAD)
  chart_version_sha="${version}+${sha}"

  helm repo add gitlab https://charts.gitlab.io/
  helm dependencies build
  helm package ./ --save --version "$chart_version_sha"
  helm serve &
  popd
  CHART_VERSION_SHA="$chart_version_sha"
  debug "Setting CHART_VERSION_SHA=$chart_version_sha"
}

install_plantuml_chart_via_helm() {
  echo -ne "${_CYN}Using helm to get chart...${_NORM}\\n"
  helm repo add gitlab 'https://charts.gitlab.io/' &&
    helm fetch --version "$CHART_VERSION" "gitlab/${CHART}" --untar --untardir "$CHARTS_DIR"
}

install_plantuml_chart_via_s3() {
  curl --fail -s "https://gitlab-charts.s3.amazonaws.com/${CHART}-${CHART_VERSION}.tgz" | tar xzC "$CHARTS_DIR"
}

echo -e "${_CYN}-- Using Chart Version: ${CHART_VERSION} --${_NORM}"

helm_opts_values=(
  "${HELM_OPTS_VALUES[@]}"
  "--set" "ingress.annotations.kubernetes\\.io/ingress\\.global-static-ip-name=plantuml-gke-$ENV"
)

# Adds annotations for auto-devops
if [[ -n ${CI:-} ]]; then
  helm_opts_values+=(
    "--set" "deployment.annotations.app\\.gitlab\\.com/app=$CI_PROJECT_PATH_SLUG"
    "--set" "deployment.annotations.app\\.gitlab\\.com/env=$CI_ENVIRONMENT_SLUG"
    "--set" "annotations.app\\.gitlab\\.com/app=$CI_PROJECT_PATH_SLUG"
    "--set" "annotations.app\\.gitlab\\.com/env=$CI_ENVIRONMENT_SLUG"
  )
fi

helm_diff() {
  debug "-- Helm Diff --" "${_CYN}"
  helm_opts=(
    "--detailed-exitcode"
    "--suppress-secrets"
    "--namespace" "$NAMESPACE"
    "--version" "$CHART_VERSION"
    "${helm_opts_values[@]}"
    "$NAME"
    "gitlab/${CHART}"
  )

  set -x +e
  helm tiller run helm diff upgrade "${helm_opts[@]}"
  RC=$?
  set +x -e
  if [[ $RC != 0 ]]; then
    debug "Changes found!" "${_YEL}"
  else
    debug "No changes found." "${_GRN}"
  fi
  debug "-------" "${_CYN}"
}

case "$ACTION" in
  install | upgrade)
    overview
    get_plantuml_chart "$CHARTS_DIR"

    helm_diff

    helm_opts=(
      "$EXTRACTED_CHARTS_DIR"
      "--namespace" "${NAMESPACE}"
      "${HELM_OPTS[@]}"
      "${HELM_OPTS_VALUES[@]}"
      "${helm_opts_values[@]}"
    )

    if [[ $ACTION == "install" ]]; then
      helm tiller run helm install --name "$NAME" "${helm_opts[@]}"
    else
      tail --pid=$$ --retry --follow "${HELM_TILLER_LOGS_DIR}" &
      helm tiller run helm upgrade "$NAME" "${helm_opts[@]}" --atomic --wait --timeout 300
    fi
    ;;

  list)
    set -x
    helm tiller run helm list
    ;;

  remove)
    overview
    warn_removal

    helm_opts=(
      "--purge" "$NAME"
      "${HELM_OPTS[@]}"
    )

    set -x
    helm tiller run helm del "${helm_opts[@]}"
    set +x
    ;;

  template)
    overview
    get_plantuml_chart "$CHARTS_DIR"
    helm_opts=(
      "$EXTRACTED_CHARTS_DIR"
      "--output-dir" "$MANIFESTS_DIR"
      "--name" "${NAME}"
      "--namespace" "$NAMESPACE"
      "${HELM_OPTS[@]}"
      "${helm_opts_values[@]}"
    )
    set -x
    helm tiller run helm template "${helm_opts[@]}"
    set +x
    ;;
esac

set +x
rm -rf "${CHARTS_DIR:?}"
