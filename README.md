# PlantUML

**Deprecation notice**: This repository has been integrated into
https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-helmfiles/-/tree/master/releases/plantuml,
and this repository is deprecated.

This repository is mirrored on https://ops.gitlab.net/gitlab-com/gl-infra/k8s-workloads/plantuml

PlantUML is a service that allows the community to create UML diagrams
from GitLab.com. It is a stateless service that provides diagrams that
are encoded in the URL.

## Configuration

### GitLab Environments Configuration

| Environment |
| ----------- |
| `pre`       |
| `gstg`      |
| `gprd`      |

### GitLab CI/CD Variables Configuration

Each variable is applied to the environment defined above

| Variable      | Default  | What it is  |
| --------      | -------- | ------------|
| `SERVICE_KEY` | None     | Key provided by the Service Account |
| `SERVICE_KEY_RO`| None   | Key provided by the Service Account |

View our common repo README for details on the above:
https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/common#gitlab-cicd-variables-configuration

### Secrets

PlantUML requires one secret `plantuml-cert` which is the SSL certificate
for the service that will be added to the L7 GCP load balancer.

Before installing the chart you must create this secret:

```
kubectl create secret tls plantuml-cert --cert pre.plantuml.gitlab-static.net.chained.crt --key pre.plantuml.gitlab-static.net.key -n plantuml
```
